
const FIRST_NAME = "Teodor";
const LAST_NAME = "Popescu";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
   
//var g={};
var cache={};

cache.pageAccessCounter=function(sectionName = 'home')
{
    sectionName = sectionName.toLowerCase();

    if(cache.hasOwnProperty(sectionName) == true)
    	cache[sectionName]++;
    else
    	cache[sectionName] = 1;
  
};

cache.getCache=function()
{
    return this;
};

return cache;

}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

